# tempest.dev

This is my development blog - I haven't updated it in
a while, but intend to come back to it and post
guides, write-ups, experiments, etc.

Unlike my other sites, this one is built by hand with
no tooling or transpilation.
