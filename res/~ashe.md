# tilde.club/~ashe/

That's this page!

This was built as something of an experiment, just to
see if I _could_.  This obviously isn't a real server
environment, but if you thought it was for a moment
then I consider this a success.

Built with:
 - [Rust](https://www.rust-lang.org/)
 - [wasm-bindgen](https://docs.rs/wasm-bindgen/)
 - [Trunk](https://trunkrs.dev/)
