# ashe.gay

This is my personal blog - I use this to post my
thoughts, experiences, and occasionally bad poetry.

Built with:
 - [React](https://reactjs.org/)
 - [NextJS](https://nextjs.org/)
 - [Gitlab Pages](https://gitlab.com/pages)
