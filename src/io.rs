use wasm_bindgen::prelude::*;

use web_sys::Element;
use web_sys::KeyboardEvent;

use crate::State;

pub enum Key {
  Letter(char),
  Space,
  Backspace,
  Return,
  Ctrl(Box<Key>),
  ArrowUp,
  ArrowDown,
  End,
}

pub fn parse_key_event(event: &KeyboardEvent, ignore_ctrl: bool) -> Option<Key> {
  if event.ctrl_key() && !ignore_ctrl {
    let base = parse_key_event(event, true)?;

    return Some(Key::Ctrl(Box::new(base)));
  }

  let key = event.key();

  match key.as_str() {
    "Tab" => {
      event.prevent_default();
      None
    },
    "Space" => Some(Key::Space),
    "Backspace" => Some(Key::Backspace),
    "Enter" => Some(Key::Return),
    "ArrowUp" => Some(Key::ArrowUp),
    "ArrowDown" => Some(Key::ArrowDown),
    "ArrowRight" => Some(Key::End),
    "End" => Some(Key::End),
    _ => {
      if key.len() != 1 {
        None
      } else {
        event.prevent_default();
        Some(Key::Letter(key.chars().next().unwrap()))
      }
    }
  }
}

// TODO: Tab completion
pub fn handle_key_event(state: &mut State, key: Key) -> Option<String> {
  match key {
    Key::Return => {
      insert_history(state);
      let input = std::mem::replace(&mut state.input, String::new());
      let prompt = build_prompt(state);

      state
        .output
        .push_back(format!("{}{}", prompt, input));

      if state.output.len() > state.max_rows {
        state.output.pop_front();
      }

      state.command_history.push(input.clone());

      return Some(input);
    }
    Key::Backspace => {
      insert_history(state);
      if state.input.len() > 0 {
        state.input.remove(state.input.len() - 1);
      }
    }
    Key::Space => state.input += " ",
    Key::Letter(letter) => {
      insert_history(state);
      let mut tmp = [0; 4];
      state.input += letter.encode_utf8(&mut tmp)
    }
    Key::Ctrl(letter) => match letter.as_ref() {
      Key::Letter(letter) => {
        if *letter == 'c' {
          insert_history(state);
          state.output.push_back(format!("{}{}^C", state.prompt, state.input));
          state.input = "".to_string();
          if state.output.len() > state.max_rows {
            state.output.pop_front();
          }
        }
      }
      _ => (),
    },
    Key::ArrowUp => {
      if !state.input.is_empty() || state.command_history.len() < 1 {
        ()
      } else if state.history_index.is_none() {
        state.history_index = Some(state.command_history.len() - 1)
      } else {
        let current_index = state.history_index.unwrap();

        if current_index > 0 {
          state.history_index = Some(current_index - 1)
        } else {
          state.history_index = Some(0)
        }
      }
    },
    Key::ArrowDown => {
      if !state.input.is_empty() || state.history_index.is_none() || state.command_history.len() < 1 {
        ()
      } else {
        let current_index = state.history_index.unwrap();
        let max_index = state.command_history.len() - 1;

        if current_index < max_index {
          state.history_index = Some(current_index + 1)
        } else {
          state.history_index = None
        }
      }
    },
    Key::End => {
      insert_history(state);
    }
    _ => ()
  };

  return None;
}

fn insert_history(state: &mut State) {
  if !state.history_index.is_none() {
    state.input = state.command_history[state.history_index.unwrap()].clone();
    state.history_index = None;
  }
}

#[wasm_bindgen(
  inline_js = "const ansi_up = new AnsiUp; export function format_html(text) { return ansi_up.ansi_to_html(text).replace(/(https:[^ )]*)/g, '<a href=\"$1\" target=\"_blank\">$1</a>') }"
)]
extern "C" {
  fn format_html(text: String) -> String;
}

pub fn print_state(target: &mut Element, state: &State) {
  let mut output = state
    .output
    .iter()
    .map(|s| (*s).clone())
    .collect::<Vec<String>>()
    .join("\n");

  if state.output.len() > 0 {
    output += "\n";
  }

  if state.prompt.len() > 0 {
    output += "\x1b[0;31m";
    output += state.prompt.as_str();
  }

  // TODO: Line overflow
  output += state.input.as_str();

  output += "\x1b[0m";

  let formatted = format_html(output);
  target.set_inner_html(formatted.as_str());
}

pub fn build_prompt(state: &State) -> String {
  let mut prompt = String::new();

  let cwd = state.cwd.replace("/home/ashe", "~");

  prompt += "\x1b[0;36m";
  prompt += "ashe";
  prompt += "\x1b[2;33m";
  prompt += "@";
  prompt += "tilde.club";
  prompt += "\x1b[2;37m";
  prompt += ":";
  prompt += "\x1b[2;32m";
  prompt += &cwd.as_str();
  prompt += "\x1b[2;37m";
  prompt += "$ ";

  if state.history_index.is_some() {
    prompt += state.command_history[state.history_index.unwrap()].as_str();
  }

  return prompt;
}
