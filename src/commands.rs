use std::str::FromStr;

use crate::State;
use crate::path;

pub fn handle_command(command_string : String, state : &mut State)  {
  let mut words = command_string.split(' ').filter(|s| !s.is_empty());

  let command = words.next().unwrap();
  let args = words.collect::<Vec<&str>>();

  match command {
    "help" => {
      state.output.push_back("implemented commands: clear, pwd, cd, ls, cat".to_string())
    }
    "pwd" => {
      state.output.push_back(state.cwd.clone())
    }

    "clear" => {
      state.output.clear();
    }

    "cd" => {
      if args.len() == 0 || args[0].is_empty() {
        state.cwd = "/home/ashe".to_string();
      } else {
        let target_path = *args.get(0).unwrap();
        let absolute_target_path = path::join(state.cwd.as_str(), target_path);

        if absolute_target_path == "/" {
          return state.output.push_back(format!("cd: permission denied: {}", args[0]))
        }

        let target = state.fs_root.get_entry_by_path(absolute_target_path.as_str());

        if target.is_err() {
          return state.output.push_back(format!("cd: no such file or directory: {}", args[0]))
        }

        let target = target.unwrap();

        if !target.is_dir() {
          return state.output.push_back(format!("cd: not a directory: {}", args[0]))
        }

        if !target.permissions.can_read {
          return state.output.push_back(format!("cd: permission denied: {}", args[0]))
        }

        state.cwd = absolute_target_path.to_string();
      }
    }

    "ls" => {
      let target_path;
      if args.len() > 0 {
        target_path = path::join(state.cwd.as_str(), args.get(0).unwrap());
      } else {
        target_path = state.cwd.clone()
      }

      let target_entry = state.fs_root
        .get_entry_by_path(target_path.as_str());

      if target_entry.is_err() {
        console_log!("Error getting path {}, error: {}", target_path, target_entry.err().unwrap().filename);
        return state.output.push_back(format!("ls: cannot access '{}': No such file or directory", target_path.as_str()))
      }

      let target_entry = target_entry.unwrap();

      if !target_entry.permissions.can_read {
        return state.output.push_back(format!("ls: permission denied: {}", args[0]))
      }

      if !target_entry.is_dir() {
        return state.output.push_back(target_entry.name.to_string())
      }

      let target = target_entry.as_dir().unwrap();
      let entries = target.get_entries();

      let output = entries.fold(String::new(), |mut a, b| {
        if b.is_dir() {
          a.push_str("\x1b[0;36m");
        }

        a.push_str(b.name);

        if b.is_dir() {
          a.push_str("\x1b[0m");
        }

        a.push_str("  ");
        a
      });

      state.output.push_back(output);
    }

    "cat" => match args.len() {
      0 => state.output.push_back(format!("cat: too few arguments")),
      1 => {
        let target_path = *args.get(0).unwrap();
        let absolute_target_path = path::join(state.cwd.as_str(), target_path);
        let target = state.fs_root.get_entry_by_path(absolute_target_path.as_str());

        if let Err(err) = target {
          return state.output.push_back(format!("cat: no such file or directory: {}", err.filename))
        }

        let target = target.unwrap();

        if !target.permissions.can_read {
          return state.output.push_back(format!("cat: {}: Permission denied", target.name))
        }

        if target.is_dir() {
          return state.output.push_back(format!("cat: {}: Is a directory", target.name))
        }

        let target_file = target.as_file().unwrap();
        output_file(target_file.contents, args[0], state)
      }
      _ => state.output.push_back(format!("cat: too many arguments")),
    }

    _ => {
      state.output.push_back(format!("bash: {}: command not found", command));
    }
  }
}

fn output_file(file_string : &str, path : &str, state : &mut State) {
  let header = "───────┬───────────────────────────────────────────────────────";
  let divider = "───────┼───────────────────────────────────────────────────────";
  let footer = "───────┴───────────────────────────────────────────────────────";

  state.print(String::from_str(header).unwrap());
  state.print(format!("       │ File: {}", path));
  state.print(String::from_str(divider).unwrap());

  let lines = file_string.split('\n');

  for (index, line) in lines.enumerate() {
    if index == 0 && line == "" {
      state.print(format!("   {:<2}  │ {}", index + 1, "\x1b[0;33m<empty>\x1b[0m"));
    } else {
      state.print(format!("   {:<2}  │ {}", index + 1, line));
    }
  }

  state.print(String::from_str(footer).unwrap());
}