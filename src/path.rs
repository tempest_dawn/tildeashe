use std::str::FromStr;

pub fn join(root : &str, path : &str) -> String {
  let mut path = String::from_str(path).unwrap();

  if path.starts_with("~/") || path.as_str() == "~" {
    path = path.replace("~", "/home/ashe");
  }

  if path.starts_with("/") {
    return path
  }

  let mut result_segments : Vec<&str> = root.split("/").filter(|s| !s.is_empty()).collect();
  let path_segments = path.split("/").filter(|s| !s.is_empty());

  for segment in path_segments {
    if segment == "." {
      continue
    }

    if segment == ".." {
      if result_segments.len() > 0 {
        result_segments.pop();
      }
      continue
    }

    result_segments.push(segment)
  }

  let result_path = result_segments.iter().fold(String::new(), |mut a, b| {
    a.reserve(b.len() + 1);
    a.push_str("/");
    a.push_str(b);
    a
  });

  return result_path
}