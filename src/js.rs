use wasm_bindgen::prelude::*;

use wasm_bindgen::JsValue;
use js_sys::Promise;

#[wasm_bindgen]
extern "C" {
  #[wasm_bindgen(js_namespace = console)]
  pub fn log(s: &str);
}

#[macro_export]
macro_rules! console_log {
  // Note that this is using the `log` function imported above during
  // `bare_bones`
  ($($t:tt)*) => (crate::js::log(&format_args!($($t)*).to_string()))
}

#[wasm_bindgen(inline_js = "export function _wait(ms) { return new Promise(res => setTimeout(res, ms)) }")]
extern "C" {
  fn _wait(ms: i32) -> Promise;
}


pub async fn wait(milliseconds: i32) -> Result<(), JsValue> {
  let promise;

  promise = _wait(milliseconds);

  wasm_bindgen_futures::JsFuture::from(promise).await?;

  Ok(())
}
