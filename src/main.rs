extern crate console_error_panic_hook;

#[macro_use]
mod js;
mod io;
mod commands;
mod fs;
mod path;

use std::collections::VecDeque;
use std::panic;
use std::str::FromStr;
use commands::handle_command;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use wasm_bindgen_futures::spawn_local;
use web_sys::Document;
use web_sys::Element;

#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

pub enum Directory {
  Home,
  Projects
}

pub struct State {
  fs_root: fs::DirEntry,
  cwd: String,
  input : String,
  output : VecDeque<String>,
  prompt: String,
  max_rows: usize,
  command_history: Vec<String>,
  history_index: Option<usize>,
}

impl State {
  pub fn new() -> Self {
    State {
      fs_root: fs::setup_fs(),
      cwd: "/home/ashe".to_string(),
      input : String::new(),
      output : VecDeque::new(),
      prompt: String::new(),
      max_rows: 24,
      command_history: Vec::new(),
      history_index: None,
    }
  }

  pub fn print(&mut self, line : String) {
    self.output.push_back(line);

    if self.output.len() > self.max_rows {
      self.output.pop_front();
    }
  }
}

fn main() {
  panic::set_hook(Box::new(console_error_panic_hook::hook));


  let window = web_sys::window().expect("should have a window in this context");
  let document = window.document().expect("window should have a document");

  spawn_local(async {
    init(document).await;
  });

}

async fn init(document : Document) {
  let mut state = State::new();

  let mut render_target = document
    .get_element_by_id("target")
    .expect("should have #target on the page");

  print_welcome(&mut render_target, &mut state).await;

  let closure = Closure::wrap(Box::new(move |event: web_sys::KeyboardEvent| {
    let key = io::parse_key_event(&event, false);

    if key.is_none() {
      return
    }

    let key = key.unwrap();

    let command = io::handle_key_event(&mut state, key);

    if let Some(command) = command {
      handle_command(command, &mut state);
    }

    state.prompt = io::build_prompt(&state);
    io::print_state(&mut render_target, &state);
  }) as Box<dyn FnMut(_)>);


  document.add_event_listener_with_callback("keydown", closure.as_ref().unchecked_ref()).unwrap();

  closure.forget();
}

pub async fn print_welcome(render_target : &mut Element, state : &mut State) {
  let intro = include_str!("../res/welcome.txt");
  let mut lines = intro.split("\n");

  let first_line = lines.next().unwrap();
  state.input = String::from_str(first_line).unwrap();
  io::print_state(render_target, state);

  js::wait(3000i32).await.unwrap();

  state.output.push_back(String::from_str(first_line).unwrap());
  state.input = String::new();

  io::print_state(render_target, state);
  js::wait(1000i32).await.unwrap();

  for line in lines {
    js::wait(30i32).await.unwrap();

    state.output.push_back(String::from_str(line).unwrap());
    if state.output.len() > state.max_rows {
      state.output.pop_front();
    }
    io::print_state(render_target, state);
  }

  js::wait(100i32).await.unwrap();

  state.prompt = io::build_prompt(&state);
  io::print_state(render_target, state);
}